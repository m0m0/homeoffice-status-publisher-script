install: script systemdunit 

systemdunit: script
	install -d $(HOME)/.config/systemd/user
	install hosc.service $(HOME)/.config/systemd/user/hosc.service
	systemctl --user daemon-reload
	systemctl --user enable hosc.service
	systemctl --user restart hosc.service
	systemctl --user status hosc.service --no-pager -l

script:
	install -d $(HOME)/.local/bin
	test -n "$(BROKER)" || ( echo "specify BROKER" && exit 1 )
	install --mode="0755" hosc.sh $(HOME)/.local/bin/hosc.sh
	sed -i "s/__BROKER__/$(BROKER)/" $(HOME)/.local/bin/hosc.sh
