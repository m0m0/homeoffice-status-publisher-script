#!/bin/bash

set -e

function setStatus {
    STATUS=$1

    IN_CONFERENCE="false"
    IS_BUSY="false"
    TIMESTAMP=$(date +%s)

    if [[ "$STATUS" == "conference" ]]; then
        IN_CONFERENCE="true"
        IS_BUSY="true"
    elif [[ "$STATUS" == "busy" ]]; then
        IS_BUSY="true"
    fi

	MSG="{ \"conference\": $IN_CONFERENCE, \"busy\": $IS_BUSY, \"timestamp\": $TIMESTAMP }"

	/usr/bin/mqtt pub -h __BROKER__ -e 30 -p 1883 --topic hos/status -m "$MSG" \
		&& echo "successfully published state '$MSG'" \
		|| echo "failed to publish state '$MSG'"
}

function update_display {
	if [ "$(pactl --format=json list source-outputs | jq ' . | length')" -gt "0" ]; then
        echo "audio source in use"
        setStatus "conference"
    elif [ `lsof /dev/video* | grep -c "/dev/video"` != "0" ]; then
        echo "video device in use"
        setStatus "conference"
    else
        echo "video and audio not recording; just 'busy'"
        setStatus "busy"
    fi
}

while sleep 10; do
    echo "=== `date` ==="
    update_display
done
